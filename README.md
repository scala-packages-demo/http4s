# http4s

[http4s/http4s](https://index.scala-lang.org/http4s/http4s) Scala interface for HTTP services. Http4s is Scala's answer to Ruby's Rack, Python's WSGI, Haskell's WAI, and Java's Servlets.